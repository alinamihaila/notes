# September 20th 2021 :cherry_blossom: 

## Focus

- Snowplow tracking for self-managed(https://gitlab.com/gitlab-org/gitlab/-/issues/337473)
- Finish remaining work from last milestone

## Tasks

### Product Intelligence

- [ ] ~~[SPIKE: Snowplow tracking in self-manage](https://gitlab.com/gitlab-org/gitlab/-/issues/337473)~~
- [x] Refine some of the issues I created

### Other

### Personal development 

- [x] Read 50 pages [Why Nations Fail: The Origins of Power, Prosperity, and Poverty](https://www.goodreads.com/book/show/12158480-why-nations-fail) 54%

- [ ] Be active everyday,  we are at the Black Sea for this week, we are taking daily walks on the beach

- [x] Book club - POODR chapter 6

## Wins and Lessons

<details>
  <summary>Expand</summary>

  #### Wins of the week

  - 


  #### What I learned

  - 

  #### What didn't go well?

  - 

</details>  

## Next week

<details>
  <summary>Expand</summary>
  
  - [ ] 

</details>


