# May 24th 2021 :cherry_blossom: 

## Focus

*  [Metrics Dictionary update and review for Usage Ping](https://gitlab.com/groups/gitlab-org/-/epics/5425)
*  [Use instrumentation classes](https://gitlab.com/gitlab-org/gitlab/-/issues/326426)
*  [Get clarity on Service Ping](https://gitlab.com/gitlab-org/gitlab/-/issues/330842)
## Tasks

### Product Intelligence

- [x] [Metrics Dictionary update and review for Usage Ping](https://gitlab.com/groups/gitlab-org/-/epics/5425)

  - [x] Weekly verification on progress

  - [x] Collaborate and do the reviews

- [ ] Continue with [POC for using metric instrumentation classes](https://gitlab.com/gitlab-org/gitlab/-/issues/326426)

  - [ ] ~~Merge proposal for generating metrics queries.~~

  - [ ] ~~Merge proposal for generating metrics suggested names.~~

### Other

### Personal development 

- [x] Continue [Thinking, Fast and Slow](https://www.goodreads.com/book/show/12385458-thinking-fast-and-slow) 20% Done

- [x] Session 1: [Building your Rest Ethic with John Fitch - 2021-05-25](https://www.youtube.com/watch?v=uklTuJeiTDo) 

- [ ] ~~Session 2: [Building your Rest Ethic with John Fitch - ~~

- [ ] ~~Read first chapter of (`Trustworthy Online Controlled Experiments (A Practical Guide to A/B Testing)`)[https://gitlab.com/gitlab-com/book-clubs/-/issues/23]~~


## Wins and Lessons

<details>
  <summary>Expand</summary>

  #### Wins of the week

  -  Progress with Metrics definitions update

  #### What I learned

  -  Merging MRs takes longer than expected after MRs are in review, I'm on my way to learn to eastimate this better.

  #### What didn't go well?

  - With a lot of changes in our group, management changes, workflow changes, project specification chnages, priorities changes, I feel I loose focuse easy.
  
</details>  

## Next week

<details>
  <summary>Expand</summary>
  
  - [ ] [Spike: Service Ping](https://gitlab.com/gitlab-org/gitlab/-/issues/331993)

</details>
