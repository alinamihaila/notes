# November 15th 2021 :cherry_blossom: 

## Focus

- e2e tests for Service ping
- FCL days for production incident

## Tasks

### Product Intelligence

- [ ] [e2e tests for Service ping](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/404)

  - [ ] Implement the most simple test so I can learn about e2e testing framework

  - [ ] Compose more scenarios
  
  - [ ] Create another MR for even a simpler case

### Other

  - [x] order webcam

### Personal development 

- [ ] 90 days of training self-confidence inspired by [video shared in #women channel](https://youtu.be/0rWmtyZXkFg?t=659)

  - [ ] positive affirmations 
  - [ ] posture
  - [ ] wording 
  - [ ] writting
  - [ ] speach
  - [ ]

- [x] Read chapter 3 of Refactoring book
- [ ] Write a blog post
- [x] Participate to Sibiu programing meetup

## Wins and Lessons

#### Wins of the week

- A couple of great pairing sessions

#### What I learned

- 

#### What didn't go well?

-

## Pages
 
- [Previoud week](2021-11-01.md)
- [Next week](2021-11-22.md)

