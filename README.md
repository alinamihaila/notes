## Introduction
 
Hello, my name is Alina Mihaila.
 
I am a Senior Backend Engineer in the Product Intelligence group, currently Acting Fullstack Engineering Manager, Product Intelligence.
 
I joined GitLab in February, 2020.
 
## Work style
 
I work mainly with the GitLab To-Do list. To reach out, mention me in a merge request or issue.
 
Reach out in [#g_product_inteligence](https://gitlab.slack.com/archives/CL3A7GFPF) group chat for questions related with [Product Intelligence](https://about.gitlab.com/handbook/engineering/development/growth/product-intelligence/).
 
My calendar is up to date, if you want to have a chat.
 
## My day
 
Timezone: UTC+2.
 
I usually start my day at 6:30am by going to the gym and after having breakfast.
 
I prefer to have focus time in the mornings, it is in general more quiet.
 
At 12:30 we have lunch with my husband and sometimes we have a walk.
 
In the afternoon continue with work and meetings.
 
On Fridays I'm preparing for the next week, check meetings, I look to take some time for learning, have training read a book.
