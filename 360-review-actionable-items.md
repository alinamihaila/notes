# 360 Feedback review

## Actionable items

- [ ] When asking for feedback, being clear in the direction what’s gonna happen 
- [ ] Assign directly if you want to delegate
- [ ] Formulate it like the others could get a quick action, vote thumbs up/down, 
  - assume people agree and ask if to mention if disagree.
- [ ] Continue working on structured and descriptive issues
- [ ] Executive Summaries for non-technical folks. Consider the audience I addres the comunication.



