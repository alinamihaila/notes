# Steps

- Code should be open for extension and close for modification.
- Have a list of questions.
- Have a list of things you don't like or don't understand

## Easy change

 > Make the change easy (warning: this may be hard), then make the easy change - Kent Beck

### Naming framework

1. Example 1

| Value(Number) | xxx?     |
| ----- | -------- |
|   1   | bottle   |
|   6   | six-pack |
|   n   | bottles  |

Possible names:

  - `number`
  - `unit` - 2 level abstract, too much
  - `category` - bottles is in unnamed category, imagine other things that fit in the same category for exampl caraf
  - `container` - a good name

2. Example 2

| Value(Number) | xxx?     |
| ---- | -------- |
|   99 | '99' |
|   50 | '55' |
|   1  | '1'  |
|   0  | 'no more'  |

Notes:

  - left column contains numbers between 99 to 0
  - right holds strings
  - expections 0 -> 'no more'

Possible names:

  - `remainder` the bottles that remain once a verse is complete
  - `name`
  - `description`
  - `quatity`

### Floking rules

1. Select the things that are most alike.
2. Find the smallest difference between them.
3. Make the simplest change that will remove that difference

### Remove a required paramter from a method

1. Rename parameter to delete_me
2. Make it optional using a default value nil
3. Remove the parameter when method is called
4. Remove the delete_me paramter

## Process

1.
