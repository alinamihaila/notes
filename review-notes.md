# Review notes and steps

Listing here a couple of notes for reviewing MRs:

* [ ] Read MR description
* [ ] Read linked issue(s) MRs
* [ ] Take notes from the reading above
* [ ] Check for MR description and suggest adding context if required. Think from the persepective of a reviewer that doesn't have the context of the changes.
This would help next reviewers or future debugging or investigations.
* [ ] Check if Docs are required
* [ ] Check if MR has correct labels

* [ ] Checkout branch
* [ ] Run tests
* [ ] Change/update tests locally with different scenarios to make a better understanding
* [ ] Check describe and context usage
* [ ] Add questions for clarifications
* [ ] Check paramters order in methods
* [ ] Method renaming, check usages of old method
* [ ] Consisteny in naming, conditionals format
* [ ] How many variants we have vs need
* [ ] Identify dependencies
   - Class names
   - Name of messages that is sent to other than self
   - Message arguments
   - Order of the arguments for a message
* [ ] When looking at code, look at complexity vs simplicity, code might not do much but have complexity.
Good code not only works, but is also simple, understandable, expressive and changeable

  1. How difficult was it to write?
  2. How hard is it to understand?
  3. How expensive will it be to change?

* [ ] Optimize for understandability, not changeability,
* [ ] Tests are not the place for abstraction, are the place for concretions

* [ ] Final note, might contain a summary of the review

## Resources

- [GitLab Code Review Guidelines](https://docs.gitlab.com/ee/development/code_review.html)
- [GitLab Database Review Guidelines](https://docs.gitlab.com/ee/development/database_review.html)

[Cyclomatic complexity, Jerry Fitzpatrick](https://en.wikipedia.org/wiki/ABC_Software_Metric)

- Assignments is a count of variable assignments.
- Branches counts not branches of an if statement (as one could forgivably infer) but branches of control, meaning function calls or message sends.
- Conditions counts conditional logic




