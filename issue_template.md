<!-- Issue template to help me improve issues descriptions -->

## Summary 

<!-- a brief description of the problem we're trying to solve or the feature/improvement we are missing. -->

## Proposal

 <!-- a highlevel concept of how this might be solved or implemented. If this is not yet known, this can be omitted. -->

## ROI (return on investment)/ Why?

<!-- 
This doesn't have to be an exact number and can be something like:
 - "this will reduce processing time", 
- "this will improve an engineer's ability to instrument metrics", 
- "this will allow viewers to find the information without going to the yml file" -->

## Requirements
<!-- 
- [ ] check box list with what this issue is fixing/implementing
- [ ]
- [ ] -->

## Severity

<!-- For non-feature issues, if the severity is known, please set it or indicate it in the description. If it's a feature or it's unknown it can be omitted. (https://about.gitlab.com/handbook/engineering/quality/issue-triage/#severity) -->
